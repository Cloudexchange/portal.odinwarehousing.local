<?php
check_odinuser();
?>
<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

   <link rel="stylesheet" href="/vendors/flatpickr/flatpickr.min.css" />

 <script>

 	


		
 function send_data(x_transporteur){
		var answer=confirm('Er word een geautomaticeerd bericht naar de vervoerder gestuurd, wil je dit ??');
       	if(answer){
   		var x_transporteur 		= 	$("#x_transporteur_select option:selected").text(); 
		var x_customer_name 	= 	$("#x_customer_name_select option:selected").text(); 			
		var fromdate 			= 	$("#fromdate").val();
		var todate 				= 	$("#todate").val();
        $.ajax({  
            url:"/scripts/send_missing_cmrs.php",  
            method:"post",  
       		data:{x_transporteur:x_transporteur,fromdate:fromdate,todate:todate,x_customer_name:x_customer_name},  
            dataType:"text",  
                 success:function(data)  
                     {  
                         alert('Bericht verzonden!');
 						load_data(x_transporteur);
                     }  
        }); 
    }
 }



   $(document).ready(function(){
  




let dropdown_customer = $('#x_customer_name_select');

dropdown_customer.empty();

dropdown_customer.append('<option selected="true" disabled>Alle klanten</option>');
dropdown_customer.prop('selectedIndex', 0);

const url_customer= '/api/api.php?request=get_portalcompanies';
//https://backoffice.odinwarehousing.com:4433/api/json_customers.php

// Populate dropdown with list of provinces
$.getJSON(url_customer, function (data) {
  $.each(data, function (key, entry) {
   
 
 

     dropdown_customer.append($('<option '+ entry.cust_number + '></option>').attr('value', entry.cust_number).text(entry.cust_name));


  })
});


});


</script>
	 
<div class="card">
	<div class="card-body">            
	    <h4 class="card-title">Missing CMR</h4> 

		<h6 class="card-subtitle">Show all missing CMR</h6>
		<div class="actions">                   
	  		<button id="btn_filterresults"  onclick="load_data()"  class="btn btn-primary ">Filter</button>
			<button id="btn_emailtransporteur" disabled  onclick="send_data()"  class="btn btn-primary ">E-Mail transporter</button>
	    </div>

			<div class="row"> 
				<div class="col-md-4">
	 				 
					 
						<select id = "x_customer_name_select" class="form-control select2-container width300">																 
							    
					</select>
					<br>		
				</div>
 
				<div class="col-md-4">

					

					<div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span></div>
                    
                    <input type="text" class="form-control date-picker hidden-sm-down flatpickr-input active"   placeholder="Selecteer een datum" id = "fromdate"  >



                    </div>

		  			<br>	

 		 			<div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span></div>
      <input type="text" class="form-control date-picker hidden-sm-down flatpickr-input active" placeholder="Selecteer een datum"  id = "todate"  >
                    </div>
 				</div>
				<br>

</div>

 <table id="data-table_table" style="display: none"; class="table table-bordered dataTable" role="grid" aria-describedby="data-table_info">
<thead class="thead-default">
  <tr>
	<th></th>								  
<th>Customer</th>							
<th>Haulier</th>
<th>License plate</th>


<th>Loadingdate</th>
<th>Loading ref.</th>
<th>Transport ref.</th>  
<th>Postal code</th>  
<th>Destination</th>  
<th>Country</th>                 
                </tr>
                </thead>
                <tbody>
    
                   
                            
              
                </tbody>
                </table>
	
<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>


   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

           <!-- Vendors: Data tables -->
        <script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src=" https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 
<script>

	
     flatpickr("#fromdate",{
     dateFormat: 'Y-m-d',
      defaultDate:new Date()
});
     flatpickr("#todate",{
     dateFormat: 'Y-m-d',
         defaultDate: new Date() // locale for this instance only
});



function load_data(){
    var org_cust_addressnr  = localStorage.getItem('org_cust_addressnr');
 

 	var x_transporteur 		= 	$("#x_transporteur_select option:selected").text(); 
		var x_customer_name 	= 	$("#x_customer_name_select option:selected").text(); 			
		var fromdate 			= 	$("#fromdate").val();
		var todate 				= 	$("#todate").val();

 
 
$('#data-table_table').show();
var table = $('#data-table_table').DataTable();
     table.destroy();


     $('#data-table_table').dataTable( {  
        "ajax": {
            "url"   : "/api/api.php?request=get_missingcrm",
            "type"  : "POST",
            "data"  : {
            "x_transporteur"  : $("#x_transporteur_select option:selected").text(),
            "x_customer_name" : $("#x_customer_name_select option:selected").text(),
            "fromdate"        : fromdate,
            "todate"          : todate,
            "cust_addressnr"  : org_cust_addressnr
        }
 


 
},


   
          //  autoWidth: !1,
          //  stateSave: true,
          //  responsive: !1,
          
            lengthMenu: [
                [25, 50, 75, -1],
                ["25 Rows", "50 Rows", "75 Rows", "Everything"]
            ],
            language: {
                searchPlaceholder: "search for records....",
                  loadingRecords: "<img style='height: 20px;'src='/img/loading.gif'> Loading......."

            },
            dom: "Blfrtip",
            buttons: [{
                extend: "excelHtml5",
                title: "Export",
                 exportOptions: {
                            columns: [1,2,3,4,5,6, 7, 8, 9, 10, 11, 12, 14,15,16,17],
                         

                        }

            }, {
                extend: "csvHtml5",
                title: "Export"
            }, {
                extend: "print",
                title: "Export"
            }],"columns": [
 
 { "data": "countrycode", render:function(data){
            return '<img src = "/img/flags/'+ data.toLowerCase() +'.gif"> ';
            }},
  
  { "data": "x_customer_name"},
  { "data": "x_transporteur"},
  { "data": "custref_out2"},
  { "data": "dt_loading"},
  { "data": "custref_out2"},
  { "data": "custref_out2"},
  { "data": "zipcode"},
  { "data": "address_1"},
  { "data": "city"} 
 
  

    ],
          
            lengthMenu: [
                [25, 50, 75, -1],
                ["25 Rows", "50 Rows", "75 Rows", "Everything"]
            ],
            language: {
                searchPlaceholder: "search for records....",
                  loadingRecords: "<img style='height: 20px;'src='/img/loading.gif'> Loading......."

            },
            dom: "Blfrtip",
            buttons: [{
                extend: "excelHtml5",
                title: "Export missing CMR"
            }, {
                extend: "csvHtml5",
                title: "Export missing CMR"
            }, {
                extend: "print",
                title: "Export missing CMR"
            }],
            initComplete: function(a, b) {
                $(this).closest(".dataTables_wrapper").prepend('<div class="dataTables_buttons hidden-sm-down actions"><span class="actions__item zmdi zmdi-print" data-table-action="print" /><span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" /><div class="dropdown actions__item"><i data-toggle="dropdown" class="zmdi zmdi-download" /><ul class="dropdown-menu dropdown-menu-right"><a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a><a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a></ul></div></div>')
            }
        }), $(".dataTables_filter input[type=search]").focus(function() {
            $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
        }), $(".dataTables_filter input[type=search]").blur(function() {
            $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
        }), $("body").on("click", "[data-table-action]", function(a) {
            a.preventDefault();
            var b = $(this).data("table-action");
            if ("excel" === b && $(this).closest(".dataTables_wrapper").find(".buttons-excel").trigger("click"), "csv" === b && $(this).closest(".dataTables_wrapper").find(".buttons-csv").trigger("click"), "print" === b && $(this).closest(".dataTables_wrapper").find(".buttons-print").trigger("click"), "fullscreen" === b) {
                var c = $(this).closest(".card");
                c.hasClass("card--fullscreen") ? (c.removeClass("card--fullscreen"), $("body").removeClass("data-table-toggled")) : (c.addClass("card--fullscreen"), $("body").addClass("data-table-toggled"))
            }
        })

     
        

 }
  </script>
            

<script>
	$(function() {

		//$("#fromdate").flatpickr();
	

	});
</script>
