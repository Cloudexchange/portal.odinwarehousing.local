
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/locale/en-gb.js"></script>
<script>
function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

</script>
<header class="content__title">
                    <h1>DELIVERED TRUCKS</h1>
                 
 
                </header>

  
 
  
</div></div>




<div class="card" style="min-HEIGHT: 520px; border-top: 8px solid #2196F3;">

  <div class="card-block">

                 
<div class="panel-body">
                            
  <table id="data-table_table" class="table table-bordered dataTable" role="grid" aria-describedby="data-table_info">
<thead class="thead-default">
  <tr>
                                    <th  ></th>
 
                        <th style="vertical-align: top;">loaded</th>
                        <th style="vertical-align: top;">warehouse ref.</th> 
                        <th style="vertical-align: top;">loading ref.</th> 
                        <th style="vertical-align: top; vertical-align: top;">customer ref 1.</th> 
                            
                        <th style="vertical-align: top;">colli</th>   
                        <th style="vertical-align: top;">colli type</th>
  <th style="vertical-align: top;">pcs</th>
                   <th style="vertical-align: top;">containernr</th>
                        <th style="vertical-align: top;">product</th>
                        <th style="vertical-align: top; text-align: right">gross mts</th>
                        <th style="vertical-align: top;">address</th>
                        <th style="vertical-align: top;">zipcode</th> 
                        <th style="vertical-align: top;">city</th> 
                              <th style="vertical-align: top;">cmr</th> 
                               <th style="vertical-align: top;">country</th>



      <th style=" vertical-align: top;">loading year</th>  
       <th style=" vertical-align: top;">loading month</th> 
         <th style=" vertical-align: top;">day</th>                
                </tr>
                </thead>
                <tbody>
    
                   
                            
              
                </tbody>
                </table>

                </div>


<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>


   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

           <!-- Vendors: Data tables -->
        <script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src=" https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 
<script>

$(document).ready(function() {
   //
  var cust_addressnr  = localStorage.getItem('cust_addressnr');   

     $('#data-table_table').dataTable( {  
        "ajax": {
            "url"   : "https://api.odinwarehousing/api/json_deliveries.php?=",
            "type"  : "GET",
            "data"  : { "cust_addressnr" : cust_addressnr
    }
},


   
          //  autoWidth: !1,
          //  stateSave: true,
          //  responsive: !1,
          
            lengthMenu: [
                [25, 50, 75, -1],
                ["25 Rows", "50 Rows", "75 Rows", "Everything"]
            ],
            language: {
                searchPlaceholder: "search for records....",
                  loadingRecords: "<img style='height: 20px;'src='/img/loading.gif'> Loading......."

            },
            dom: "Blfrtip",
            buttons: [{
                extend: "excelHtml5",
                title: "Export",
                 exportOptions: {
                            columns: [1,2,3,4,5,6, 7, 8, 9, 10, 11, 12, 14,15,16,17],
                         

                        }

            }, {
                extend: "csvHtml5",
                title: "Export"
            }, {
                extend: "print",
                title: "Export"
            }],"columns": [
 

 { "data": "countrycode", render:function(data){
            return '<img src = "/img/flags/'+ data.toLowerCase() +'.gif"> ';
            }},
  

 { "data": "dt_loading",   type : "date" },



 { "data": "warehouseref", render:function(data){
            return '<a href = "/warehouseref/'+ data + '">'+ data + '</a>';
            }},
  

{ "data": "out_ordnr"},
{ "data": "custref_out1"},
{ "data": "carrier"},
{ "data": "carriercode"},
{ "data": "outer_pck"},
{ "data": "containernr"},
{ "data": "prodref1"},

{ "data": "grossweight", render:function(data){
            return number_format(data / 1000, 3, ',', '.');
            }},

 
{ "data": "address_1"},
{ "data": "zipcode"},
{ "data": "city"},

 { "data": "cmrid", render:function(data){
    if(data==0){
return '-';
    }else{
            return '<a  target="_blank" href = "http://backoffice.odinwarehousing.com:8089/cmr/'+data+'.pdf"> <img src = "/img/PDF-128.png" style="height: 20px"></a> ';
            }

            }},
 
{ "data": "countrycode"},
{ "data": "year"},
{ "data": "month"},
{ "data": "day" },


    ],"columnDefs": [

    { "visible": false, "targets": 15},
 { "visible": false, "targets": 16 },
    { "visible": false, "targets": 17 },
{ "visible": false, "targets": -1 },


  ],
            initComplete: function(a, b) {
                $(this).closest(".dataTables_wrapper").prepend('<div class="dataTables_buttons hidden-sm-down actions"><span class="actions__item zmdi zmdi-print" data-table-action="print" /><span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" /><div class="dropdown actions__item"><i data-toggle="dropdown" class="zmdi zmdi-download" /><ul class="dropdown-menu dropdown-menu-right"><a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a><a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a></ul></div></div>')
            }
        }), $(".dataTables_filter input[type=search]").focus(function() {
            $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
        }), $(".dataTables_filter input[type=search]").blur(function() {
            $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
        }), $("body").on("click", "[data-table-action]", function(a) {
            a.preventDefault();
            var b = $(this).data("table-action");
            if ("excel" === b && $(this).closest(".dataTables_wrapper").find(".buttons-excel").trigger("click"), "csv" === b && $(this).closest(".dataTables_wrapper").find(".buttons-csv").trigger("click"), "print" === b && $(this).closest(".dataTables_wrapper").find(".buttons-print").trigger("click"), "fullscreen" === b) {
                var c = $(this).closest(".card");
                c.hasClass("card--fullscreen") ? (c.removeClass("card--fullscreen"), $("body").removeClass("data-table-toggled")) : (c.addClass("card--fullscreen"), $("body").addClass("data-table-toggled"))
            }
        })

    } );
  </script>
    
