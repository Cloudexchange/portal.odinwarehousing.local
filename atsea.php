 


<header class="content__title">
                    <h1>AT SEA</h1>
                 
 
                </header>

 
 



                          <div class="card" style="min-HEIGHT: 520px; border-top: 8px solid #9E9E9E;">

  <div class="card-block">

                 
<div class="panel-body">
                            
  <table id="data-table_table" class="table table-bordered dataTable" role="grid" aria-describedby="data-table_info">
<thead class="thead-default">
    <tr>                                  
            <th>vessel</th>
            <th>bill of lading</th>  
            <th>eta rtm</th>
            <th>Colli</th>

        </tr>
                </thead>
                <tbody>
    
                   
                            
              
                </tbody>
                </table>

                </div>


<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>


 

           <!-- Vendors: Data tables -->
        <script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>

 
<script>

$(document).ready(function() {
   //
  var cust_addressnr  = localStorage.getItem('cust_addressnr');   

 

             $("#data-table_table").DataTable({
            autoWidth: !1,
            stateSave: true,
            responsive: !1,
            ajax: 'http://api.odinwarehousing.com/api/json_atsea.php?cust_addressnr='+ cust_addressnr,
            lengthMenu: [
                [25, 50, 75, -1],
                ["25 Rows", "50 Rows", "75 Rows", "Everything"]
            ],
            language: {
                searchPlaceholder: "search for records....",
                  loadingRecords: "<img style='height: 20px;'src='/img/loading.gif'> Loading......."

            },
            dom: "Blfrtip",
            buttons: [{
                extend: "excelHtml5",
                title: "Export"
            }, {
                extend: "csvHtml5",
                title: "Export"
            }, {
                extend: "print",
                title: "Export"
            }],
            initComplete: function(a, b) {
                $(this).closest(".dataTables_wrapper").prepend('<div class="dataTables_buttons hidden-sm-down actions"><span class="actions__item zmdi zmdi-print" data-table-action="print" /><span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" /><div class="dropdown actions__item"><i data-toggle="dropdown" class="zmdi zmdi-download" /><ul class="dropdown-menu dropdown-menu-right"><a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a><a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a></ul></div></div>')
            }
        }), $(".dataTables_filter input[type=search]").focus(function() {
            $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
        }), $(".dataTables_filter input[type=search]").blur(function() {
            $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
        }), $("body").on("click", "[data-table-action]", function(a) {
            a.preventDefault();
            var b = $(this).data("table-action");
            if ("excel" === b && $(this).closest(".dataTables_wrapper").find(".buttons-excel").trigger("click"), "csv" === b && $(this).closest(".dataTables_wrapper").find(".buttons-csv").trigger("click"), "print" === b && $(this).closest(".dataTables_wrapper").find(".buttons-print").trigger("click"), "fullscreen" === b) {
                var c = $(this).closest(".card");
                c.hasClass("card--fullscreen") ? (c.removeClass("card--fullscreen"), $("body").removeClass("data-table-toggled")) : (c.addClass("card--fullscreen"), $("body").addClass("data-table-toggled"))
            }
        })

    } );
  </script>
    
