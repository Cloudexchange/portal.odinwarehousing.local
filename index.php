<?php
//Logout
if(isset($_GET['logout'])){
 session_destroy();
  header("index.php");
}

error_reporting();
include "config.php";
include "include/functions.php";
//checkjwt();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Vendor styles -->
        <link rel="stylesheet" href="/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
 
        <link rel="stylesheet" href="/bower_components/sweetalert2/dist/sweetalert2.min.css">
            <link rel="stylesheet" href="/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
        <link rel="stylesheet" href="/css/app.min.css">
    </head>
        <style>
    body {
    background: url('img/haven.jpg');
    background-repeat: no-repeat;
 background-size: cover;
 
}
        </style>
  

                            <!-- Vertically centered -->
                            <div class="modal fade" id="modal-forgotpassword" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title pull-left">Password reset</h5>
                                        </div>
                                        <div class="modal-body">
                                            Enter your email address to receive a password reset mail. Follow the link in the email to generate a new password for your account.

                                 
                                  
<br><br>
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email"  id="txt_emailaddress" name= "txt_emailaddress" class="form-control" placeholder="name@example.com">
                                        <i class="form-group__bar"></i>
                                    </div>


                               


                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="btn_sendpassword" class="btn btn-link">Send</button>
                                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                              <body data-ma-theme="grey">
      
        <div class="login" style="filter: drop-shadow(0 0 0.75rem #CCC);" >

    
    
    
   <div class="card animated fadeIn">
                        <div class="card-body" style="padding: 40px;">
             <center> <img src="img/logo.png" alt="Odin warehousing logo" width="300px" style="margin-bottom: 10px; ">
              
                       <form id="formlogin" method="POST">  
</center>
                    <div class="form-group form-group--float form-group--centered">
                        <input style="text-align: left" type="text" name="login-email" id="login-email" onkeypress="return searchKeyPress(event);"  class="form-control"  placeholder="Username..">
                    </div>

                    <div class="form-group form-group--float form-group--centered">
<input style="text-align: left" type="password" name="login-password" id="login-password" onkeypress="return searchKeyPress(event);"   class="form-control" placeholder="Password..">
                    </div>      
           <div id="result-container"></div>
              <button id = "btn_login" name= "btn_login" type="button" class="btn btn-primary btn-block">Login</button>
             <br>
              </div>  
                </form>
       
                </div>
            </div>

            

             
            </div>

 
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
//

$(function() {
    
 
    $( "#btn_forgotpassword" ).click(function() {
     $('#modal-forgotpassword').modal('toggle');
    });


  $( "#btn_sendpassword" ).click(function() {
     
 

       $.ajax({
            url: '/actions/actions_login.php',
            type: 'POST',
            data: JSON.stringify({
                emailaddress:    $('#txt_emailaddress').val(),
                action: 'sendpasswordreset'
            }),
            success: function (data) {

 

if(data.errorcode==0){

swal({
                    title: 'Information!',
                    text: 'A confirmation email has been sent to your registered email address, which you should receive in a few minutes. Please follow the instructions in this email to reset your password.',
                    type: 'info',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary'
                })
    $('#modal-forgotpassword').modal('toggle');
 
}else{

   swal({
                    title: 'error!',
                    text: 'E-mailadress not found.',
                    type: 'error',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary'
                })
}





               }
           
      
      //  return false;
  


  });








    });



});
</script>
      
<script>

function searchKeyPress(e)
  {
    // look for window.event in case event isn't passed in
    e = e || window.event;
    if (e.keyCode == 13)
    {
      document.getElementById('btn_login').click();
     
      return false;
    }
    return true;
  }

$(function () {


$( "#btn_login" ).click(function() {



var str = $( "#formlogin" ).serialize();
 var username =  $('#login-email').val();
 var password =  $('#login-password').val();
 

//$.post( "test.php", { 'choices[]': [ "Jon", "Susan" ] } );

 
      // login
        $.ajax({
            url: '/api/api.php?request=login',
            type: 'POST',
            data: {
                username: $('#login-email').val(),
                password: $('#login-password').val()
            },
            success: function (data) {
             
  
if(data.error_code=='A1009'){

 

swal({
                    title: 'Error!',
                    text: data.error_message,
                    type: 'error',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary'
                })
}else{

 
//localStorage.setItem('jwt', data.jwt);
localStorage.setItem('cust_addressnr', data.cust_addressnr);
localStorage.setItem('userid', data.id);
localStorage.setItem('org_cust_addressnr', data.cust_addressnr);
window.location.href = "dashboard.php";
 
}

            },
            error: function () {

             
            }
        });

        return false;
    });

   
});
</script>

  
              <!-- Vendors -->
        <script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="/vendors/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="/vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

    

        <script src="/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>


        <!-- Vendors: Data tables -->
        <script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>

        <!-- App functions and actions -->
        <script src="/js/app.min.js"></script>


    </body>
</html>

