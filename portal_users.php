 
<header class="content__title">
                    <h1>Portal users</h1>
            

                <div class="actions">
                      <a href="/portal_user/"></button>  <button style="width: 130px;" id="btn_save" type="button" class="btn pull-right btn-success btn--icon-text "><i class="zmdi zmdi-plus"></i>Add user</button> </a>
                    </div>
                </header>
              
   <link rel="stylesheet" href="//bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="//bower_components/sweetalert2/dist/sweetalert2.min.css">
            <link rel="stylesheet" href="/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">

<div class="card">
                        <div class="card-block">
 

                                          
          
                       
 
<table id="data-table_data" class="table table-bordered dataTable" role="grid" aria-describedby="data-table_info">
<thead class="thead-default">
<tr role="row">


  <th>name</th>
  <th>username</th>
  <th>customername</th>
<th>Admin</th>
<th>active</th>
  <th>last login</th>
 
 
</tr>
</thead>
<tbody>
                               
  

 </tbody>
                            </table> 
        
           </div>
           </div>
           
           
           

<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>      
<script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
     
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <form id = "formdata" > 
    <div class="row">

 <input type="hidden"  class="form-control" id="txt_id" name="txt_id">
 <input type="hidden"  class="form-control" id = "cust_addressnr" value="<?PHP echo $cust_addressnr; ?>" name="cust_addressnr">



<div class="col-md-6">
  <div class="form-group">
    <label>Username</label>
    <input  type="text" class="form-control" id="txt_username" name="txt_username">
  </div>


  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" id="txt_name" name="txt_name">
  </div>

<div class="checkbox">
<input type="checkbox"  value="1" id="chk_admin" name="chk_admin">
<label class="checkbox__label" >Admin</label>
</div>

<div class="checkbox">
<input type="checkbox" value="1" id="chk_active" name="chk_active">
<label class="checkbox__label" >Active</label>
</div>

 
</div>

<div class="col-md-6">

    <div class="form-group">
    <label>Password</label>
    <input type="text" class="form-control" id="txt_password1" name="txt_password1">
  </div>



</div>
 
</form>
       </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id = "btn_saveuser" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

 
<script>


$(document).ready(function() {
   //
  var cust_addressnr  = localStorage.getItem('cust_addressnr');   

 
 $('#data-table_data').dataTable( {
  "ajax": {
    "url": "https://backoffice.odinwarehousing.com:4433/api/json_users.php",
     "type": "POST",
    "data": {
                "cust_addressnr" : cust_addressnr,
                                "sysadmin" : true
    }
  }
 ,  

 

     
            lengthMenu: [
                [25, 50, 75, -1],
                ["25 Rows", "50 Rows", "75 Rows", "Everything"]
            ],
            language: {
                searchPlaceholder: "search for records....",
                  loadingRecords: "<img style='height: 20px;'src='/img/loading.gif'> Loading......."

            },
            dom: "Blfrtip",
            buttons: [{
                extend: "excelHtml5",
                title: "Export"
            }, {
                extend: "csvHtml5",
                title: "Export"
            }, {
                extend: "print",
                title: "Export"
            }],
            initComplete: function(a, b) {
                $(this).closest(".dataTables_wrapper").prepend('<div class="dataTables_buttons hidden-sm-down actions"><span class="actions__item zmdi zmdi-print" data-table-action="print" /><span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" /><div class="dropdown actions__item"><i data-toggle="dropdown" class="zmdi zmdi-download" /><ul class="dropdown-menu dropdown-menu-right"><a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a><a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a></ul></div></div>')
            }
        }), $(".dataTables_filter input[type=search]").focus(function() {
            $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
        }), $(".dataTables_filter input[type=search]").blur(function() {
            $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
        }), $("body").on("click", "[data-table-action]", function(a) {
            a.preventDefault();
            var b = $(this).data("table-action");
            if ("excel" === b && $(this).closest(".dataTables_wrapper").find(".buttons-excel").trigger("click"), "csv" === b && $(this).closest(".dataTables_wrapper").find(".buttons-csv").trigger("click"), "print" === b && $(this).closest(".dataTables_wrapper").find(".buttons-print").trigger("click"), "fullscreen" === b) {
                var c = $(this).closest(".card");
                c.hasClass("card--fullscreen") ? (c.removeClass("card--fullscreen"), $("body").removeClass("data-table-toggled")) : (c.addClass("card--fullscreen"), $("body").addClass("data-table-toggled"))
            }
        })

    } );


 

  </script>
    