<?php
/*
   * Api Return file
   *
   * @author      Jeff Timmer <info@wecareforit.nl>
   * @company     Odin Warehousing
   * @website     www.wecareforit.nl
   * @date        Januari 2019
 */
 
include "config.php";


error_reporting(0);
$pdo = new PDO('mysql:host=' . $servername . ';dbname=' . $database . '', $username, $password);
$pdo->exec("set names utf8");



header('Access-Control-Allow-Origin: *');
session_start();

$action     = !empty($_POST['action']) ? $_POST['action'] : '';


 
if ('GET' === $_SERVER['REQUEST_METHOD']) {
       echo json_encode(array(
                  'error_code'    => A1001,
                  'error_message' => "Method GET is NOT allowed"
            ));
   exit();
}




 
  if(trim(empty($action))){
    echo json_encode(array(
                  'error_code'    => A1002,
                  'error_message' => "No valid action found in POST data"
            ));
   exit();
  };
 



  $allow = array("62.45.185.53","136.144.201.113","83.128.251.25","127.0.0.1"); 
  //62.45.185.53      Kantoor we care for IT
  //136.144.201.113   VPS Server Plesk
  //83.128.251.25     Jeff Thuis
  //127.0.0.1         Localhost

  if(!in_array($_SERVER['REMOTE_ADDR'], $allow)) {
    echo json_encode(array(
                  'error_code'    => A1003,
                  'error_message' => "IP Adress not allow to use this api"
            ));
   exit();
  };
 

//Define al function 
if($action=='get_missingcmr'){
 
header('Content-Type: application/json');

try{

   $x_transporteur = $_POST['x_transporteur'];
  $todate = $_POST['todate'];
  $fromdate = $_POST['fromdate'];
 $x_customer_name =  $_POST['x_customer_name'];

  
$fdate = explode("-", $fromdate);
$fromdate_eng = implode("-", array_reverse($fdate));

$tdate = explode("-", $todate);
$todate_eng = implode("-", array_reverse($tdate));


$statement  = $pdo->prepare("SELECT * FROM import_outtake WHERE statuscode = 'CONFIRMED' AND x_transporteur <> 'FCA (niet laden zonder kenteken)' AND CITY NOT IN ('Maasvlakte Rotterdam','Rotterdam-Maasvlakte','Maasvlakte','Rotterdam / Maasvlakte','Maasvlakte / Rotterdam') AND x_customer_name = '$x_customer_name' AND dt_loading BETWEEN '$fromdate_eng' AND '$todate_eng' AND out_ordnr NOT IN (SELECT filename FROM attachments)");


$statement->execute();
$array      = $statement->fetchAll( PDO::FETCH_ASSOC );
 
 

echo  '{"data":  ';
echo json_encode($array);
echo  ' }';

}
  catch(PDOException $e)
  {
    echo json_encode(array(
    'error_code'     => "A1004",
    'error_message'  => "Fout bij het ophalen van missing CMR's.",                  
    ));

  }

}




