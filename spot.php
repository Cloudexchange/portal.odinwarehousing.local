
<?PHP



$id = $_GET['url2'];
$id_storage = $_GET['url3'];

if($id){
 
$data = $db->rawQueryOne("SELECT
  `parking_spots`.`storage_location_id` AS `storage_id`,
  `storage_locations`.`name` AS `storage_name`,
  `parking_spots`.`reserved_till`,
  `parking_spots`.`tenant_id`,
    `parking_spots`.`id` as parking_spot_id,


    `storage_locations`.`id`,
    `parking_spots`.`status_id`,
    `parking_spots`.`description`,
  `parking_spots`.`parking_type_id` AS `parking_types_id`,
  `parking_types`.`name` AS `parking_types_name`
FROM
  `parking_spots`
  INNER JOIN `storage_locations` ON `parking_spots`.`storage_location_id` =
`storage_locations`.`id`
  INNER JOIN `parking_types` ON `parking_spots`.`parking_type_id` =
`parking_types`.`id`
WHERE   `parking_spots`.`id` = $id
");


if ($db->count == 0){
  echo '<div class="alert alert-warning" role="alert">
                                Er is geen data gevonden met het opgegeven ID.
                            </div>';
  //exit();
}


}

 
 

switch ($data['status_id']) {
    case 1:
$status = 'Verhuurbaar';
        break;
    case 2:
$status = 'Onverhuurbaar';
        break;
     case 3:
$status = 'Niet opgeleverd';
        break;
 
    default:
$status = 'Onbekend';
}

 
 
?>


              


<div class="card">
<div class="card-block">
<div class="row">
<div class="col-md-4">
  <h4 class="card-title"><?php if($id){ echo $data['description'] . " | " . $data['storage_name']; }else{  echo "Plaats toevoegen"; } ?></h4><br>
 
</div>
<div class="col-md-8"     >
<div style="float: right">
<a  href="/location/<?php echo $data['storage_id']; ?>"><?PHP echo $data['storage_name']; ?>ert</a>
</div>
</div>
</div>
                            
                      
   
  
           
      <form   id = "form" method="POST">  


        <input value="<?PHP echo $id; ?>" name="id" type="hidden" class="form-control" >
         <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                <label>Naam</label> 

                
<input  value="<?PHP echo $data['storage_location_id']; ?>" hidden type="text" name="storage_location_id" class="form-control" >
                  <input value="<?PHP echo $data['description']; ?>" name="description" type="text" class="form-control" >
                <i class="form-group__bar"></i>
              </div>
            </div>

             <div class="col-md-4">
            <div class="form-group">
                <label>Status</label> 
                <select class="form-control" name="status_id">

               
  
                   <option value="1">Verhuurbaar</option>
                  <option value="2">Onverhuurbaar</option>                  
                  <option value="3">Niet opgeleverd</option>
   <option selected value="<?PHP echo $data['status_id']; ?>"><?PHP echo $status; ?></option>
</select>
              </div>
            </div>
<div class="col-md-4">
            <div class="form-group">
                <label>Type</label> 
                  <select name= "parking_type_id" class="form-control">
                              <?PHP
                $types = $db->get ("parking_types");
                foreach ($types as $type) { 
                echo "<option value = ".$type['id']." '>".$type['name']."</option>";
              }
                ?>
                   <option selected value="<?PHP echo $data['parking_types_id']; ?>"><?PHP echo $data['parking_types_name']; ?></option>
                 </select>
              </div>
            </div>

          </div>


 
  <div class=" row">
    <div class=" col-md-12">
  <button class="btn btn-primary btn--icon-text"  style="width:  120px;"  type="submit" id = "btn_save"><i class="zmdi zmdi-save"></i>Opslaan</button>  


 <button class="btn btn-danger btn--icon-text"  style="width:  120px;"  onClick="history.go(-1); return false;"  ><i class="zmdi zmdi-arrow-left"></i>Terug</button>     

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>        
</div>
</form>


           </div>

         </div> <script>
   /*--------------------------------------
                Bootstrap Notify Notifications
            ---------------------------------------*/
            function notify(from, align, icon, type, animIn, animOut){
                $.notify({
                    icon: icon,
                    title: ' Bootstrap Notify',
                    message: 'Turning standard Bootstrap alerts into awesome notifications',
                    url: ''
                },{
                    element: 'body',
                    type: type,
                    allow_dismiss: true,
                    placement: {
                        from: from,
                        align: align
                    },
                    offset: {
                        x: 15, // Keep this as default
                        y: 15  // Unless there'll be alignment issues as this value is targeted in CSS
                    },
                    spacing: 10,
                    z_index: 1031,
                    delay: 2500,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: false,
                    animate: {
                        enter: animIn,
                        exit: animOut
                    },
                    template:   '<div data-notify="container" class="alert alert-dismissible alert-{0} alert--notify" role="alert">' +
                                    '<span data-notify="icon"></span> ' +
                                    '<span data-notify="title">{1}</span> ' +
                                    '<span data-notify="message">{2}</span>' +
                                    '<div class="progress" data-notify="progressbar">' +
                                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                    '</div>' +
                                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                                    '<button type="button" aria-hidden="true" data-notify="dismiss" class="alert--notify__close">Close</button>' +
                                '</div>'
                });
            }
$(document).ready(function(){

         $(function(){
            $("#form").submit(function(event){
     

                event.preventDefault();
                $.ajax({
                    method: 'POST',
                    url: '/include/save_spot.php',
                    data : $('#form').serialize(),
                    success: function(data){
                swal({
                    title: 'successvol',
                    text: 'De gegevens zijn successvol opgeslagen ',
                    type: 'success',
                    showCancelButton: false,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success',
                    confirmButtonText: 'Sluiten!',
                    cancelButtonClass: 'btn btn-secondary'
                }).then(function(){
                    window.location.replace("/location/<?PHP echo $data['storage_id']; ?>");
                });    


                    },
                    error: function(xhr, desc, err){
                        alert(err);
                    }
                });
            });
        });
    });

</script>

</div>




<?PHP
 
   
$data_contract = $db->rawQuery("SELECT
  `contracts`.`parking_spot_id`,
  `parking_spots`.`description` AS `parking_spot_name`,
  `parking_spots`.`id` AS `parking_spot_id`,
  `contracts`.`active`,
  `contracts`.`id`,
  `contracts`.`rent_end_date`,
  `contracts`.`rent_cancellation_date`,
  `contracts`.`cancelled`,
  Date_Format(`contracts`.`rent_start_date`, '%d-%m-%Y') AS
`rent_start_date_nl`,
  `contracts`.`tenant_id`,
  `contracts`.`send_key_date`,
  Date_Format(`contracts`.`receive_key_date`, '%d-%m-%Y') AS
`receive_key_date_nl`,
  `contracts`.`receive_key_date`,
  `tenants`.`name`,
  `tenants`.`address`,
  `tenants`.`postalcode`,
  `tenants`.`city`,
  `tenants`.`phone`
FROM
  `contracts`
  INNER JOIN `parking_spots` ON `contracts`.`parking_spot_id` =
`parking_spots`.`id`
  INNER JOIN `tenants` ON `contracts`.`tenant_id` = `tenants`.`id`
WHERE
  `contracts`.`parking_spot_id` = $id ");



  ?>

<div class="card">
  <div class="card-block">
     <h4 class="card-title">Contracten</h4><br>
 <table class="table table-bordered dataTable" role="grid" >
<?PHP
if ($db->count == 0){
echo "Geen contracten gevonden voor deze klant ";
}else{
?>
<thead class="thead-default">
  <tr>        
<th>#</th>
<th>Locatie</th>    
<th>Klantnaam</th> 
<th>Start datum</th>
<th>Sleutel verstuurd</th>
<th>Opzegdatum</th>
<th>Eind datum</th>
<th>Sleutel ingeleverd</th>
<th>Actief</th>


</tr>
 </thead>
<tbody>
  <?PHP 

foreach ($data_contract as $contract) {
switch ($contract['active']) {
      case 1:
$active = '<span style="width: 100px;"  class="badge badge-success" >Actief</span>';
        break;
    case 0:
$active = '<span style="width: 100px;"  class="badge badge-danger" >Niet actief</span>';
        break;
}

 

echo "<tr>"; 
echo "<td>".$contract['id']."</td>";
echo "<td>".$contract['parking_spot_name']."</td>";
 echo "<td><a href ='/tenant/".$contract['tenant_id']."'>".$contract['name']."</a><br><small>".$contract['address'].", ".$contract['postalcode']."</small></td>";

echo "<td>".makenull($contract['rent_start_date_nl'])."</td>";
echo "<td>".date2nldate($contract['send_key_date'])."</td>";
echo "<td>".date2nldate($contract['rent_cancellation_date'])."</td>";
echo "<td>".date2nldate($contract['rent_end_date'])."</td>";
echo "<td>".makenull($contract['receive_key_date_nl'])."</td>";
echo "<td>".$active."</td>";
echo '<td  style="width:100px;" class="text-right">
<div class="btn-group">
<a href = "/contract/'.$contract['id'].'"><button style="width:100px;" class="btn btn-primary btn--icon-text"><i class="zmdi zmdi-edit"></i> Wijzig</button></a></td>';



  echo "</tr>"; 

}


?>

  </tr>
</tbody>
</table>
          <?PHP } ?>                          

       </div>

     </div>