<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>


    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">

<script>
$(document).ready(function() {


  var cust_addressnr  = localStorage.getItem('cust_addressnr');   
 

    $.ajax({
  url: 'http://api.odinwarehousing.com/api/api.php',
  type: 'POST',
  data: {
  action: "get_userinfo",
  userid: "<?PHP echo $_GET['url2']; ?>",
    cust_addressnr: cust_addressnr
},
  success: function (data) {

 
if(data === ''){
    $("#pagetitle" ).html("Add user");

}else{
var json = $.parseJSON(data);
$("#pagetitle" ).html("Edit user "  + json.name);
  $("#txt_username").attr("disabled", "disabled");
}
 
 if(json.cust_addressnr != cust_addressnr)
 {
 window.location = "/users/";

 }

 //ss
$("#txt_name").val(json.name)
$("#txt_emailaddress").val(json.emailaddress)
$("#txt_username").val(json.username)
$("#txt_id").val(json.id);
$("#select_admin").val(json.admin).change();
$("#select_active ").val(json.active).change();
 

  },
  error: function () {
  }
});
 


$( "#btn_save" ).click(function() {
////alert(  );
//Check formdata


var formdata = $( "#form_edituser" ).serialize();
var cust_addressnr  = localStorage.getItem('cust_addressnr');   


 $.ajax({
  url: 'http://api.odinwarehousing.com/api/api.php',
  type: 'POST',
data: {
    action:         "save_user",
    id:             $("#txt_id").val(),
    name:           $("#txt_name").val(),
    username:       $("#txt_username").val(),
    password:       $("#txt_password").val(),
    active:         $("#select_active").val(),
    emailaddress:   $("#txt_emailaddress").val(),
    admin:          $("#select_admin").val(),
    cust_addressnr: cust_addressnr
},
  success: function (data) {
    var json = $.parseJSON(data);
 
 if(json.errorstatus == 1){
    swal({
                        title: 'Error!',
                        text: json.message,
                        type: 'error',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary'
    });

 }else{
    swal({
                        title: 'Success!',
                        text: json.message,
                        type: 'success',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary'
    }).then(function() {
    window.location = "/users/";
});;
    
 


 }


    


 


  },
  error: function () {
  }
});
 


 









});



    } 


    );






 </script>







<header class="content__title">
                    <h1 id = "pagetitle">Edit user</h1>

                <div class="actions">
            

                        <button style="width: 130px;" id="btn_back" onclick="window.history.back()" type="button" class="btn pull-right btn-primary btn--icon-text"><i class="zmdi zmdi-arrow-back"></i>back</button>  <button style="width: 130px;" id="btn_save" type="button" class="btn pull-right btn-success btn--icon-text "><i class="zmdi zmdi-save"></i>Save</button> 
                    </div>
                </header>

<div class="row">


<div class="col-md-12">
  <div class="card" >
<div class="card-block">


<form id = "form_edituser">


<div class="row">
<div class="col-md-6">


   <div class="form-group">

    <div class="form-group">

         <input type="hidden"id = "txt_id" name = "txt_id"   >

 <div class="form-group">
        <label>Username</label>

                                   
                                        <input type="text"   id = "txt_username" name = "txt_username" class="form-control" >
                                        <i class="form-group__bar"></i>


                                    </div>

        <label>Name</label>

                                        <input type="text" id = "txt_name" name = "txt_name" class="form-control" >
                                        <i class="form-group__bar"></i>
                                    </div>

   
    <div class="form-group">

        <label>Group</label>
   <select id = "select_admin" name = "select_admin" class="form-control" >
              <option value="">Users</option>
                                      <option value="2">Administrators</option>
                        
                                  </select>
                                    </div>
    <div class="form-group">

        <label>Status</label>
                 <select id = "select_active" name = "select_active" class="form-control" >
                                      <option value="1">Enable</option>
                                      <option value="0">Disable</option>
                                  </select>

                                    </div>
        </div>

   


</div> 
 <div class="col-md-6">

 
   <div class="form-group">
        <label>Emailaddress</label>

                                
                                        <input type="text" id = "txt_emailaddress" name = "txt_emailaddress" class="form-control" >
                                        <i class="form-group__bar"></i>
                                    </div>


   <div class="form-group">
        <label>Password</label>

                                     
                                        <input type="text" id = "txt_password" name = "txt_password" class="form-control" >
                                        <i class="form-group__bar"></i>
                                    </div>



   <div class="form-group" id="div_sysadmin" style="display: none">

        <label>Customer addressnr</label>

                                     
                                        <input type="text" id = "txt_cust_addressnr" name = "txt_cust_addressnr" class="form-control" >
                                        <i class="form-group__bar"></i>
                                    </div>



</div></form>

  </div>
 
          
                </div>
		


<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>


 

           <!-- Vendors: Data tables -->
        <script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>

  