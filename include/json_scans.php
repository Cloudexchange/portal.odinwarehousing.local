<?PHP
header('Content-Type: application/json');
include_once "../classes/database/MysqliDb.php";
include_once "../config.php";
include_once "../JsonUtils.php";
include_once "../include/checktoken.php";

$db = new MysqliDb ($servername, $username, $password, $database);  
//error_reporting(0);

$json = $db->rawQuery(" SELECT
  `import_address`.`name_1`,
  `scans`.`datetime`,
  `scans`.`outordnr`,
  `scans`.`jobnumber`,
  `scans`.`module`,
  `scans`.`type`,
  `scans`.`barcode`,
  `scans`.`warehouseref`
FROM
  `scans`
  INNER JOIN `intake_outtake` ON `scans`.`warehouseref` =
`intake_outtake`.`warehouseref`
  INNER JOIN `import_address` ON `intake_outtake`.`cust_addressnr` =
`import_address`.`addressnr`
  GROUP BY `scans`.`barcode`
ORDER BY
  `scans`.`id` DESC
");

 echo  '{"data": [ ';
  foreach ($json as $json_result) {    
    $json_data =  '[
      "'.$json_result['warehouseref'].'",
      "'.$json_result['name_1'].'",
      "'.$json_result['type'].'",
      "'.$json_result['barcode'].'",
      "'.$json_result['outordnr'].'",
      "'.$json_result['jobnumber'].'",
      "'.$json_result['datetime'].'"
    ],';

$json_data_new .= $json_data; 

}
echo substr($json_data_new, 0, -1);
echo  '] }';
   
 
 
?>

 
 