<?php

include "../vendor/autoload.php";
include "../include/functions.php";

$jwt_token      =  $_SESSION['jwt'];
use \Firebase\JWT\JWT;
 
    try {
        $decoded            =       JWT::decode($_SESSION['jwt'], 'this-is-the-secret', array('HS256'));
        $decoded_array      =       (array) $decoded;
        $user_name          =       $decoded_array['name'];
        $user_email         =       $decoded_array['email']; 

        if(empty($decoded_array)){
          header("location: ../index.php"); 
        }else{
           $user_name       =    $decoded_array['name'];
           $user_email      =    $decoded_array['email']; 
        }
    } catch (Exception $e) {
        header("location: ../index.php");
    }
 
 
?>