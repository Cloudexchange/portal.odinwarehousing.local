<?PHP
header('Content-Type: application/json');
include_once "../classes/database/MysqliDb.php";
include_once "../config.php";
include_once "../JsonUtils.php";
include_once "../include/checktoken.php";
 
$db = new MysqliDb ($servername, $username, $password, $database);  
error_reporting(0);
 


$cust_addressnr		=		$_GET['ca'];
$con=mysqli_connect($servername, $username	,$password	,$database	);

//Deliveries
$c_deliveries_planned = mysqli_num_rows(mysqli_query($con,"SELECT SUM(carrier_out)as carrier_out_sum FROM intake_outtake  WHERE cust_addressnr = '$cust_addressnr' AND statuscode_out='PLANNED' AND released = 2 GROUP BY out_ordnr "));

$c_deliveries = mysqli_num_rows(mysqli_query($con,"SELECT SUM(carrier_out)as carrier_out_sum FROM intake_outtake  WHERE cust_addressnr = '$cust_addressnr' AND statuscode_out = 'CONFIRMED'  GROUP BY out_ordnr"));

//Stock
$c_stock = mysqli_query($con,"SELECT sum(colli) as tonstock ,sum(grossweight) as tgross  FROM current_stock WHERE customernr = '$cust_addressnr'");

$row_stock 		= mysqli_fetch_assoc($c_stock);
$c_tstock 		=  $row_stock['tonstock'];
$c_tgross 		=  number_format($row_stock['tgross'] / 1000, 3, ',', '.');

 

$c_carriers_sea_colli = mysqli_num_rows(mysqli_query($con,"SELECT sum(carrier) as count_carriers  FROM  sea_fright_import WHERE cust_addressnr = '$cust_addressnr' AND statuscode = 'PLANNED'"));

$c_carriers_sea_containers = mysqli_num_rows(mysqli_query($con,"SELECT * FROM sea_fright_import WHERE cust_addressnr = '$cust_addressnr' AND statuscode = 'PLANNED'"));

$json_data = "";
$json_data 		= json_addfield( $json_data,"c_deliveries_planned",$c_deliveries_planned);
$json_data 		= json_addfield( $json_data,"c_deliveries",$c_deliveries);
$json_data 		= json_addfield( $json_data,"c_tstock",$c_tstock);
$json_data 		= json_addfield( $json_data,"c_tgross",$c_tgross);
$json_data 		= json_addfield( $json_data,"c_carriers_sea_colli",$c_carriers_sea_colli);
$json_data 		= json_addfield( $json_data,"c_carriers_sea_containers",$c_carriers_sea_containers);


$json_data 		= json_closefieldlist( $json_data);
$jsondata 		= json_add($jsondata, $json_data) ;

		echo $jsondata;

?>

 
 