	<?PHP
	function format_size($size) {
	      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	      if ($size == 0) { return('n/a'); } else {
	      return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
	}

	$db = new MysqliDb ($servername, $username, $password, $database); 	 
	$db->GroupBy($cust_addressnr,$cust_addressnr);

	if($cust_addressnr=='125'){
		$where = "";
	}else{
		$where  = "WHERE cust_addressnr = '$cust_addressnr'"; 
		
	}
 
	$rows = $db->rawQuery("SELECT
		`cmr`.`cmr`,
		`intake_outtake`.`out_ordnr`,
		`intake_outtake`.`custref_in1`,
		`intake_outtake`.`custref_in2`,
		`intake_outtake`.`dt_loading`,
		`intake_outtake`.`warehouseref`,
		`intake_outtake`.`bill_of_ladingnr`,
		`intake_outtake`.`cust_addressnr`,
		`intake_outtake`.`city`,
		`intake_outtake`.`zipcode`,
		`intake_outtake`.`countrycode`
	FROM
	  `cmr`
	  INNER JOIN `intake_outtake` ON `cmr`.`cmr` = `intake_outtake`.`out_ordnr`
	  $where
	");

?> 
	

<div class="card" style="min-HEIGHT: 520px; border-top: 8px solid #2196F3;">
	<div class="card-block">            
	    <h4 class="card-title">Show all avaible CMSRs Found (<?php echo $db->count; ?>) CMR's</h4>
		<table id="data-table" class="table table-bordered table-striped">
       	<thead class="thead-default">
		    <tr>
			    <th></th>											  
			    <th></th>	
				<th>warehousref.</th>
				<th>loading ref.</th>
				<th>customer ref 1.</th>
				<th>customer ref 2.</th>
				<th>Postalcode</th> 
				<th>City</th> 
				<th></th>
			</tr>
		</thead>
        <tbody>
           	<?PHP
				if ($db->count > 0){
					foreach ($rows as $row) {
						$cmrfile	=	'/uploads/cmr/'.$row['out_ordnr'].'.pdf';
						$flagcode 	= 	strtolower($row['countrycode']);			
						$flagpath 	= 	"../img/flags/".$flagcode.".gif"; 
							echo "
							    <tr>
									<td style='width: 20px'><img src='../img/PDF-128.png' style='height: 16px;'></a></td>
									<td><a target='_blank' href = '$cmrfile'>Download</a> ( ". format_size(filesize($cmrfile)) . ")</td>
									<td>".$row['warehouseref']."</td>
									<td>".$row['out_ordnr']."</td>
									<td>".$row['custref_in1']."</td>
									<td>".$row['custref_in2']."</td>
									<td>".$row['zipcode']."</td>
									<td>".$row['city']."</td>
									<td style='width: 50px' ><img  src = ".$flagpath."   ></td>
								</tr>
							";   
					}
				}
			?>              
        </tbody>
    </table>
	</div>
</div>

<script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/js/app.min.js"></script>				