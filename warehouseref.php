<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<script>


         var jwt = localStorage.getItem('jwt');
         var cust_addressnr  = localStorage.getItem('cust_addressnr');
         var org_cust_addressnr  = localStorage.getItem('org_cust_addressnr');

            $(document).ready(function(){

 

 
              $.ajax({
            url: 'https://backoffice.odinwarehousing.com:4433/api/api.php',
            type: 'POST',
            data: {
                token: jwt,
                action: "get_warehouserefinfo",
                warehouseref : '<?PHP echo $_GET['url2']; ?>',
                cust_addressnr : cust_addressnr
            },


            success: function (data, statusTxt, xhr) {  
 

 var json    = $.parseJSON(data);//parse JSON
 
var intakedata = json.intake;
 

for (var i = 0; i < intakedata.length; i++) {
    var object = intakedata[i];
  
$('#intake_table').append('<tr><td>' + object.dt_discharge + '</td><td>' + object.carrier+'</td><td>' + object.carriercode+'</td><td>' + object.outer_pck+'</td><td>' + object.grossweight+'</td> </tr>');
 

}

 
var stockdata = json.stock;
 
if(stockdata == ''){ 
 $("#stock_card").html('No stock availible');

 }else{
$("#stock_table").fadeIn("slow"); 
for (var i = 0; i < stockdata.length; i++) {
    var object = stockdata[i];
  
$('#stock_table').append('<tr><td>' + object.x_productcode + '</td><td>' + object.colli + '</td><td>' + object.pieces + '</td><td>' + object.grossweight + '</td></tr>');
 
}
}


  $("#outtake_card").html('');
var outtakedata = json.outtake;

if(outtakedata == ''){ 
 $("#outtake_card").html('No outtake availible');

 }else{
$("#outtake_table").fadeIn("slow"); 

 
for (var i = 0; i < outtakedata.length; i++) {
    var object = outtakedata[i];
  
$('#outtake_table').append('<tr><td><img src = "/img/flags/'+ object.countrycode.toLowerCase() +'.gif"></td> <td>' + object.dt_loading + '</td><td> ' + object.carrier + '</td><td>' + object.carriercode + '</td><td>' + object.grossweight + '</td><td>' + object.city + '</td> </tr>');
 
//  loaded  colli colli type  pcs gross city
}}



var releasedata = json.outtake;

if(releasedata == ''){ 
 $("#release_card").html('No release availible');

 }else{
$("#release_table").fadeIn("slow"); 

 
for (var i = 0; i < releasedata.length; i++) {
    var object = releasedata[i];
  
$('#release_table').append('<tr><td><img src = "/img/flags/'+ object.countrycode.toLowerCase() +'.gif"></td> <td>' + object.dt_loading + '</td><td> ' + object.carrier + '</td><td>' + object.carriercode + '</td><td>' + object.grossweight + '</td><td>' + object.city + '</td> </tr>');
 
//  loaded  colli colli type  pcs gross city
}}  
          },
            error: function (xhr, ajaxOptions, thrownError) {
  
      }
        });

        return false;
    });





</script>

        <link rel="stylesheet" href="vendors/bower_components/lightgallery/dist/css/lightgallery.min.css"><?PHP
$warehouseref = $_GET['url2'];
 
?>

<header class="content__title">
                    <h1>Warehouse reference <?PHP echo $_GET['url2']; ?></h1>
                 
 
                </header>

                <div class="row">
                    <div class="col-md-6">
                           <div class="card" style="min-height: 400px;">
                            <div class="card-block">
                                <h4 class="card-title">intake</h4>

<table id="intake_table" class="table table-striped ">
                                <thead>
                                    <tr>
                                       
                                              <th>in warehouse </th>
                                                    
                                              <th>colli</th> 
                                                      <th>colli type</th> 
                                                  <th>pcs</th> 
                                    
                                              <th >gross</th> 
                                
                                               
                                              
                     
                                                                                          
                                    </tr>
                                </thead>
                                <tbody>
    
 


</tbody>
                </table>
                
 
 

                             
                            </div>
                        </div>
                    </div>

      <div class="col-md-6">
                                  <div class="card" style="height: 400px;">
                            <div class="card-block">
                                <h4 class="card-title">outtake</h4>
                                           <div id="outtake_card"></div>
<table id="outtake_table" style="display: none" class="table table-striped ">
                                <thead>
                                    <tr>
                                       <td></td>
                                              <th>in warehouse </th>
                                                    
                                              <th   >colli</th> 
                                                      <th >colli type</th> 
                                                  <th  >gross</th> 
                                    
                                              <th   >city</th> 
                                
                                               
                                              
                     
                                                                                          
                                    </tr>
                                </thead>
                                <tbody>
    
 


</tbody>
                </table>
                
 
 
                             
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card" style="min-height: 400px;">
                            <div class="card-block" >
                                <h4 class="card-title">stock</h4>
                                 <div id="stock_card"></div>
 <table id="stock_table" style="display: none;" class="table table-striped ">
                                <thead>
                                    <tr>
                                       
                                    
                        <th>product</th>

                      
                     <th >colli in</th>
        
                     <th  >pcs</th>                       
                     <th  >gross mts</th>
                                                                                          
                                    </tr>
                                </thead>
                                <tbody>  
  </tbody>
                </table> 
                
 
                             
                            </div>
                        </div>
                    </div>

      <div class="col-md-6">
                                     <div class="card" style="min-height: 400px;">
                            <div class="card-block">
                                <h4 class="card-title">released</h4>
                                 
  <div id="release_card"></div>
 <table id="release_table" style="display: none;" class="table table-striped ">
                                <thead>
                                    <tr>
                                       
                        <th>loaded </th>
                        <th>colli</th> 
                        <th>colli type</th> 
                        <th>gross</th> 
                        <th>to</th>   
                                                                                          
                                    </tr>
                                </thead>
                                <tbody>  
  </tbody>
                </table> 
                             
                            </div>
                        </div>
                    </div>




 <div class="col-md-12">
                           



  
<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>      
<script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
         <!-- Light Gallery -->
        <script src="/vendors/bower_components/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script>
$(document).ready(function() {
    var table = $('#data-table_data').DataTable( {
                   ajax: '/include/json_getphotos.php?oudordnr=<?PHP echo $oudordnr; ?>',
                    "pageLength": 50,
        "columnDefs": [  {
                "targets": [ 0 ],
                "visible": false,
            }
            
              ]


    } );
 

   



    $('#data-table_data tbody').on( 'click', '#button_check', function () {
        var data = table.row( $(this).parents('tr') ).data();
        var id = data[0];
       
  $.get("../include/update_photo.php?tk=APTOKENODIN&action=activephoto&id=" + id, function(data, status){
        // alert("Data: " + id + "\nStatus: " + status);
 
$('#data-table_data').DataTable().ajax.reload();


    });




 //button_check_off



 

 
    } )

     
    $('#data-table_data tbody').on( 'click', '#button_checkoff', function () {
        var data = table.row( $(this).parents('tr') ).data();
       var id = data[0];
 
  $.get("../include/update_photo.php?tk=APTOKENODIN&action=deactivephoto&id=" + id, function(data, status){
        // alert("Data: " + id + "\nStatus: " + status);
 
$('#data-table_data').DataTable().ajax.reload();


    });



    } );
    

     ;
} );

 
 

   </script>