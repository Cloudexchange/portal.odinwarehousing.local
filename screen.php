<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Vendor styles -->
        <link rel="stylesheet" href="/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="//bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="//bower_components/sweetalert2/dist/sweetalert2.min.css">
        <!-- App styles -->
        <link rel="stylesheet" href="/css/app.min.css">
        <script src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<style>
html, body {
  overflow:hidden;
  height:100%;
}
#div_footer{
    background-color: #2196f3;
    height:  50px;
    width: 100%;
    position:   absolute;
    bottom: 0px;
    color: white;
    line-height: 48px;
    text-align: center;
    font-size: 20px;

}
#div_top{
    position:   absolute;
    top:0px;
    height:180px;
    padding: 10px;
    width:  100%;
    color: white;
}
h5,h4,h2{
    color: white;
    font-weight:    normal;
    font-size: 20px;

}
#div_content{
    position: absolute;
overflow:auto;width:200px;top: 210px; height:70%; 
    width: 99%;
    margin: 10px; 
 
}#div_planned{
  
    height: 60px;
    border-left: 5px solid #2196F3;
    padding: 10px;
    background-color: white;

    margin-bottom:  10px;
    width: 100%
}
#div_content::-webkit-scrollbar { 
    display: none; 
}
#div_loading{

    height: 60px;
    border-left: 5px solid #FF9800;
    padding: 10px;
    background-color: white;

    margin-bottom:  10px;
    width: 100%
}

#div_finished{

    height: 60px;
    border-left: 5px solid #32c787;
    padding: 10px;
    background-color: white;

    margin-bottom:  10px;
    width: 100%
}

</style>
<body onLoad="scrollDiv_init()">
    </head>


<script>
$(function() {
 getstats();

setInterval(function(){ 

 getstats()

}, 10000);

 
       
});


ScrollRate = 100;

function scrollDiv_init() {
    DivElmnt = document.getElementById('div_content');
    ReachedMaxScroll = false;
    
    DivElmnt.scrollTop = 0;
    PreviousScrollTop  = 0;
    
    ScrollInterval = setInterval('scrollDiv()', ScrollRate);
}

function scrollDiv() {
    
    if (!ReachedMaxScroll) {
        DivElmnt.scrollTop = PreviousScrollTop;
        PreviousScrollTop++;
        
        ReachedMaxScroll = DivElmnt.scrollTop >= (DivElmnt.scrollHeight - DivElmnt.offsetHeight);
    }
    else {
        ReachedMaxScroll = (DivElmnt.scrollTop == 0)?false:true;
        
        DivElmnt.scrollTop = PreviousScrollTop;
        PreviousScrollTop--;
    }
}

function pauseDiv() {
    clearInterval(ScrollInterval);
}

function resumeDiv() {
    PreviousScrollTop = DivElmnt.scrollTop;
    ScrollInterval    = setInterval('scrollDiv()', ScrollRate);
}


 
    



function getstats(){

//Get the footer counter
$.getJSON("https://backoffice.odinwarehousing.com:4433/api/api.php?action_test=get_screenfooter", function(result){

    $.each(result, function(i, field){

    $("#count_p").html(result.count_planned);
    $("#count_p").fadeIn("fast"); 

    $("#count_l").html(result.count_loading);
    $("#count_l").fadeIn("fast"); 

    $("#count_f").html(result.count_finished);
    $("#count_f").fadeIn("fast"); 

    $("#count_c").html(result.count_confirmed);
    $("#count_c").fadeIn("fast"); 

    });

});


  $.getJSON("https://backoffice.odinwarehousing.com:4433/api/api.php?action_test=get_screencontenttop", function(result){

        $.each(result, function(i, field){

         $("#date_day_1").html(result.date_day_1);        
         $("#count_cars_1").html(result.count_cars_1)
         $("#count_colli_1").html(result.count_colli_1)

         $("#name_date_2").html(result.name_day_2);
         $("#date_day_2").html(result.date_day_2);        
         $("#count_cars_2").html(result.count_cars_2)
         $("#count_colli_2").html(result.count_colli_2)


         $("#name_date_3").html(result.name_day_3);
         $("#date_day_3").html(result.date_day_3);        
         $("#count_cars_3").html(result.count_cars_3)
         $("#count_colli_3").html(result.count_colli_3)


         $("#name_date_4").html(result.name_day_4);
         $("#date_day_4").html(result.date_day_4);        
         $("#count_cars_4").html(result.count_cars_4)
         $("#count_colli_4").html(result.count_colli_4)


         $("#name_date_5").html(result.name_day_5);
         $("#date_day_5").html(result.date_day_5);        
         $("#count_cars_5").html(result.count_cars_5)
         $("#count_colli_5").html(result.count_colli_5)


         $("#name_date_6").html(result.name_day_6);
         $("#date_day_6").html(result.date_day_6);        
         $("#count_cars_6").html(result.count_cars_6)
         $("#count_colli_6").html(result.count_colli_6)
       

            });
        });


//get al the content  $.getJSON("https://backoffice.odinwarehousing.com:4433/api/api.php?action_test=get_screencontenttop", function(result){

    $("#div_content").load("https://backoffice.odinwarehousing.com:4433/api/api.php?action_test=get_screencontent").fadeIn("fast"); ;



$("#name_date1").html('VANDAAG');




}

</script>
<div id = "div_top">

<div class="row">
    <div class="col-md-2">
        <div class="card bg-green" style="height: 180px">
            <div class="card-body">
                <h2  style="margin-bottom: 10px;" id = "name_date1"></h2>
                <div style ="margin: 0px;" id="date_day_1"></div><br>
                <div style="float: left">
                    <h5 class="md-title nomargin">Auto's</h5>
                    <h4 class="nomargin" id = "count_cars_1"></h4>
                </div>
                <div style="float: right;">
                    <h5 class="md-title nomargin">Colli</h5>
                    <h4 class="nomargin" id = "count_colli_1"></h4>
                </div>
            </div>
        </div>
    </div>

 
    <div class="col-md-2">
        <div class="card bg-blue" style="height: 180px"> 
            <div class="card-body">
                <h2  style="margin-bottom: 10px;" id = "name_date_2"></h2>
                <div style ="margin: 0px;" id="date_day_2"></div><br>
                <div style="float: left">
                    <h5 class="md-title nomargin">Auto's</h5>
                    <h4 class="nomargin" id = "count_cars_2"></h4>
                </div>
                <div style="float: right;">
                    <h5 class="md-title nomargin">Colli</h5>
                    <h4 class="nomargin" id = "count_colli_2"></h4>
                </div>
            </div>
        </div>
    </div> 

    <div class="col-md-2">
        <div class="card bg-blue" style="height: 180px"> 
            <div class="card-body">
                <h2  style="margin-bottom: 10px;" id = "name_date_3"></h2>
                <div style ="margin: 0px;" id="date_day_3"></div><br>
                <div style="float: left">
                    <h5 class="md-title nomargin">Auto's</h5>
                    <h4 class="nomargin" id = "count_cars_3"></h4>
                </div>
                <div style="float: right;">
                    <h5 class="md-title nomargin">Colli</h5>
                    <h4 class="nomargin" id = "count_colli_3"></h4>
                </div>
            </div>
        </div>
    </div> 

    <div class="col-md-2">
        <div class="card bg-blue"  style="height: 180px"> 
            <div class="card-body">
                <h2  style="margin-bottom: 10px;" id = "name_date_4"></h2>
                <div style ="margin: 0px;" id="date_day_4"></div><br>
                <div style="float: left">
                    <h5 class="md-title nomargin">Auto's</h5>
                    <h4 class="nomargin" id = "count_cars_4"></h4>
                </div>
                <div style="float: right;">
                    <h5 class="md-title nomargin">Colli</h5>
                    <h4 class="nomargin" id = "count_colli_4"></h4>
                </div>
            </div>
        </div>
    </div> 


    <div class="col-md-2">
        <div class="card bg-blue" style="height: 180px"> 
            <div class="card-body">
                <h2  style="margin-bottom: 10px;" id = "name_date_5"></h2>
                <div style ="margin: 0px;" id="date_day_5"></div><br>
                <div style="float: left">
                    <h5 class="md-title nomargin">Auto's</h5>
                    <h4 class="nomargin" id = "count_cars_5"></h4>
                </div>
                <div style="float: right;">
                    <h5 class="md-title nomargin">Colli</h5>
                    <h4 class="nomargin" id = "count_colli_5"></h4>
                </div>
            </div>
        </div>
    </div> 

        <div class="col-md-2">
        <div class="card bg-blue"  style="height: 180px"> 
            <div class="card-body">
                <h2  style="margin-bottom: 10px;" id = "name_date_6"></h2>
                <div style ="margin: 0px;" id="date_day_6"></div><br>
                <div style="float: left">
                    <h5 class="md-title nomargin">Auto's</h5>
                    <h4 class="nomargin" id = "count_cars_6"></h4>
                </div>
                <div style="float: right;">
                    <h5 class="md-title nomargin">Colli</h5>
                    <h4 class="nomargin" id = "count_colli_6"></h4>
                </div>
            </div>
        </div>
    </div> 


                
</div>

</div>

<div id="div_content">
   

</div>


<div id = "div_footer">
    <div class="row">
        <div class="col-md-2">        
        </div>
        <div class="col-md-2" style="border-right:  1px solid white ">
            Planned: <span id="count_p"></span>
        </div>
        <div class="col-md-2"  style="border-right:  1px solid white ">
            Loading: <span id="count_l"></span>
        </div>
        <div class="col-md-2" >
            Confirmed: <span id="count_c"></span>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div


</html>
